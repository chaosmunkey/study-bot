#!/usr/bin/env python3

"""

"""

# STDLIB IMPORTS
from os import environ
from os.path import dirname, join

# THIRD PARTY IMPORTS
from discord import Client
from discord.channel import VoiceChannel
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)
_bot_token = environ["BOT_TOKEN"]




client = Client()

@client.event
async def on_ready():
    """

    """
    print(f"Logged in as: {client.user}")
    # print(client.guilds[0].channels)

@client.event
async def on_voice_state_update(member, before, after):
    """
    Let's deal with user joining and leaving a
    voice channel.
    """
    voice_channel = None
    leaving = None
    # if the before channel is set, then the user
    # left the voice channel.
    if before.channel:
        print(f"User: {member} has left...")
        voice_channel = before.channel
        leaving = True
    else:
        print(f"User: {member} has joined...")
        voice_channel = after.channel
        leaving = False

    print(type(voice_channel) == VoiceChannel)



client.run(_bot_token)
